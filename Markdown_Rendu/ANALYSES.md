

# ANALYSES TEXTOMETRIQUES

#### À propos du projet

Dans le cadre du cours de Programmation et Projet Encadré du semestre 2 du master TAL, nous nous sommes intéressées à l'analyse des flux RSS.

Nous avons décidé de travailler sur 4 Classes afin de réaliser les analyses textométriques sur notre Corpus : PLANETE - ECONOMIE -INTERNATIONAL- POLITIQUE

La question était d'analyser les thèmes principaux au cours de cette année d'en dégager les grandes actualités et tendances et de voir si nous faisons face à des périodes plus particulières que d'autres.

- Dans un premier temps nous avons fait avec les paramètres par défauts : # Filter out words that occur less than 20 documents, or more than 50% of the documents.
  dictionary.filter_extremes(no_below=20, no_above=0.5)

- Dans un deuxieme temps (avec les résultats dans un nouveau dossier) des nouveaux paramètres : Filter out words that occur less than 15 documents, or more than 30% of the documents.
  dictionary.filter_extremes(no_below=15, no_above=0.3) 

## CATÉGORIE PLANETE

- **Janv à avril - Hiver**

Pour la classe planète nous avons analysé les adjectifs + noms sur les lemmes.

On note pour cette période que la répartition des tokens est assez équilibrée par thème car elle tourne autour de 10% par topic. S'agissant des thèmes 1  et 2 on note les tokens relatifs au gaz et à l'électricité.

![hiver_planete_NomAdj.png](./hiver_planete_NomAdj.png)

- **Mai à août - Été**
  
  On constate que comme pour le reste de notre corpus nous avons toujours le mot "monde" en premier car c'est le plus fréquent dans le corpus également dans la classe "planète". 

A travers le Top-30 MOST SALIENT TERMS nous pouvons voir que les lemmes "température, eau, chaleur, gaz, feu, énergie, environnement" sont les 30 premiers termes les plus fréquent cela nous montre bien que dans la classe planète les enjeux environnementaux comme le réchaufement climatique sont très abordés.

![plan-ete-lemme-1.png](./plan-ete-lemme-1.png)

Nous avons voulu voir pour le TOPIC 1 quels lemmes revenaient le plus et on constate que les lemmes "département, risque, été, secteur" mais aussi " "biodiversité, terre" sont très présents. En période estivale les départements ont dû faire face au réchauffement climatique connu durant l'année 2022 et donc mettre en place des mesures de crise face au "risque" des températures.

![plan-ete-lemme-TP1.png](./plan-ete-lemme-TP1.png)

Regardons les noms + adjectifs pour les formes on retrouve les mêmes premiers termes cependnat nous retrouvons pour les formes le lemme "guerre", qui nous redonne également les enjeux politique notamment avec la pénurie de gaz sur la période estivale.

![plan-ete-forme-1.png](./plan-ete-forme-1.png)

TOPIC 1 :

![plan-ete-formes-param1.png](./plan-ete-formes-param1.png)

Lorsque l'on regarde les noms propres pour la classe planète sur la période été on observe que les nomps propres les plus importants de notre corpus sont "météo-france", "gironde", "afrique", "landiras". 

Les problèmes de chaleur se font ressentir dans ces termes car Landiras avait été victime d'un incendie l'été dernier qui avait ravagé l'exploitation d'un apiculteur. La Gironde avait également connu de fortes chaleurs. On constate ici encore l'urgence climatique qui est soulevée depuis l'été dernier.

![plan-ete-lemme-NP1.png](./plan-ete-lemme-NP1.png)

Pour les verbes on peut retrouver le verbe "interdire", qui est assez intéressant car il soulève des mesures prises face aux problèmes certainement liés aux incendies, et au réchauffement climatique et notamment les problèmes de gaz qui avient été soulevés pour anticiper les pénuries de l'hiver 2022.

![pan-ete-lemme-verb.png](./pan-ete-lemme-verb.png)

- **Septembre à Décembre - Automne**

Pour la période automne on constate que la préocupation principale concerne étrangement le "passe" sanitaire. Nous pouvons supposer qu'un nouveau variant avait été trouvé à cette période.

![plan-hiver-lemme-1.png](./plan-hiver-lemme-1.png)

Lorsque l'on observe le Topic 7 on retrouve le lemme "variant" avec "cas" en fréquence élevée.

![plan-hiver-TP7.png](./plan-hiver-TP7.png)

En jouant sur les paramètres et en restreignant nos paramètres on peut oberver deux noms propres principaux qui confirment nos hypothèses, on constate bien que la Covid-19 était revenue au sein de l'actualité avec le variant "omicron"

![plan-hiver-NP.png](./plan-hiver-NP.png)

### 

## CLASSE ÉCONOMIE

- **Janv à avril - Hiver**

Pour l'analyse sur les lemmes ainsi que sur les formes concernat les POS noms + adjectifs, avec les différents paramètres nous constatons que le lemme "monde" est celui qui apparaît le plus dans notre corpus dans le TOP-30 Salient Terms. En deuxième position nous avons le lemme "tribune". 

Pour les formes :

![eco-aut-formes-1.png](./eco-aut-formes-1.png)

Pour les lemmes :

![Capture d’écran 2023-05-19 à 21.44.25.png](./eco-aut-1.png)

Lorsque que l'on se concentre sur les lemmes on peut donc voir que dans notre Topic 1 nous avons en première position le lemme "monde" qui a une fréquence proche de 100.

![eco-aut-lemme-2.png](./eco-aut-lemme-2.png)

Cependant dans notre catégorie "économie", nous pouvons supposer que le terme "monde" est forcément très employé et ne nous donne donc pas d'indications très précises si ce n'est que nous avons un corpus international.

Pour cela nous nous sommes interessées aux autres lemmes les plus fréquents, et nous avons vu "inflation, crise, prix, million, hausse" qui sont les plus représentés dans les Topics 2 à 4, sûrement dû aux problèmes économiques rencontrés face à la gueurre en Ukraine car nous avons la présence du terme "crise", ces sujets ont été très présents ces derniers mois dans plusieurs pays.

![eco-aut-lemme-3.png](./eco-aut-lemme-3.png)

On retrouve également une forte présence du mot "tribune", dans le TOPIC 5 avec une fréquence proche de 80 et rappelons que dans notre corpus global le terme apparaissait en deuxième position donc nous savons qu'il est sur représenté dans le TOPIC 5, certainement dû a une allocution politique dans cette période de l'année. 

![eco-aut-lemmes-4.png](./eco-aut-lemmes-4.png)

Toujours dans le TOPIC 5 le mot "groupe" est en deuxième position, puis dans les TOPIC 6 à 10 nous avons "gouvernement, contrôleur, grève, économie", tout cela nous montre bien l'évolution des principales difficultés économiques rencontrées au cours de l'année 2022. 

Nous avons également modifié les paramètres afin de voir avec une plus grande fréquence si les lemmes trouvés varient beaucoup et nous constatons qu'il n'y a pas vraiment de différences significatives.

![eco-aut-param1.png](./eco-aut-param1.png)

- **Mai à août - Été**

Intéressons-nous maintenant à la période de Mai à Août 2022 que nous avons considéré comme étant l'été.

En ananalysant les lemmes pour les noms + adjectifs nous retrouvons encore le lemme "monde" en première position dans le TOPIC 1 et surepresenté dans notre corpus global. Il en est de même pour les formes.

![eco-ete-lemme-1.png](./eco-ete-lemme-1.png)

Pour les formes :

![eco-ete-forme-1.png](./eco-ete-forme-1.png)

Dans le TOPIC 1 nous retrouvons les mots "crise, coût, risque ", mais nous avons l'apparition des lemmes "électricité, consommateur, chaleur, débat". 

![eco-ete-lemme-TP1.png](./eco-ete-lemme-TP1.png)

Dans les autres TOPIC nous avons les termes "travail, secteur, revenu, véhicule,impact", ce qui nous montre que les préocupations salariales et d'emploi étaient plus présentes à partir de cette période. Nous avons également à partir du TOPIC 6 les lemmes "gaz, syndicat, patron, jeune, urgence".

![eco-ete-lemmes-TP6.png](./eco-ete-lemmes-TP6.png)

Le sujet de la hausse du GAZ suite à la gueurre en Ukraine et les questions économiques notamment pour les "jeunes" et les étudiants se sont fait ressentir fortement. A partir du TOPIC 8 nous avons le mot "environnement", puis également "transport,pratique, hausse,taux",ménage, famille". Nous pouvons donc voir qu'il s'agit plus de l'inquiétude de la hausse des prix au niveau du gaz, du coût de la vie et de la répercussion que cela peut avoir au niveau de l'environnement et des foyers car nous avons les termes "foyers".

En changeant les paramètres de metrics nous trouvons le mot "salarié"" en plus dans les lemmes avec ce changement de paramètres.

Nous remarquons étonnamment que le terme "carburant" est vraiment très peu présent dans le corpus. Il est présent mais en très petite fréquence face aux autres lemmes cités plus haut.

Regardons maintenant les résultats que nous obtenons pour les noms propres.

On peut voir que sur les lemmes ou bien sur les formes les résultats sont identiques. Pour les noms propres on retrouve France en premier lieu, car il s'agit d'un journal français, puis nous avons les Noms Propres "escande, etats-unis, chine,philippe, paris, europe et ukraine". Cela nous révèle bien les principaux sujets avec la guerre en Ukraine et ces sujets sont liés aux principales puissances mondiales. On retrouve le prénom Philippe certainement pour Edouard Philippe.

![eco-aut-NP-1.png](./eco-aut-NP-1.png)

Lorsque l'on regarde les verbes on voit que les verbes : "devoir, avoir, pouvoir, faire," sont les principaux verbes de notre corpus. Nous constatons que ce sont des verbes d'action et de devoir, beaucoup utilisés dans le domaine politique et également économique.

![eco-verb-lemme-1.png](./eco-verb-lemme-1.png)

On peut voir selon les TOPICS que les verbes principaux changent cependant certains verbes sont plus présents dans notre corpus car ils sont également des verbes très utilisées comme le verbe "avoir" avec l'auxiliaire en français, car nous somme dans l'analyse des lemmes.

C'est pourquoi nous avons décidé d'**analyser les formes sur les verbes** afin de voir quel forme des verbes nous avons exactement.

![eco-verb-formes-1.png](./eco-verb-formes-1.png)

À ce stade on constate donc que nous avons la forme "devrait" pour le verbe avoir cela nous précise bien que l'on parle de devoir et non pas de l'auxiliaire avoir. On a ensuite les forme "augmenter,doit, doivent, faire, expliquer, annoncé etc.". Cela nous cofirme bien que l'on parle de sujets économiques avec le verbe augmenter, mais cela peut également être lié au rechauffement climatique comme nous avons vu le terme "environnment" précédémment. Mais nous avons toujours le verbe devoir avec les formes "doit,doivent". On parle bien ici de responsabiltés et de devoir face aux sujets économiques. 

- **Septembre à décembre - Automne**

Pour les noms et adjectifs sur la période de Septembre à Décembre, nous retrouvons encore une fois le lemme "monde" mais en deuxième position, car avant lui nous retrouvons le lemme "pays" puis en troisième position nous avons le lemme "président" puis "guerre". Lorsque l'on observe les formes nous avons les mêmes premiers termes que pour les lemmes dans le Top-30 Most Salient Terms.

Pour les lemmes des noms + adjectifs

![eco-hiver-lemme1.png](./eco-hiver-lemme1.png)

Pour les formes des noms + adjectifs 

![eco-hiver-formes1.png](./eco-hiver-formes1.png)

Pour les lemmes dans le TOPIC 1 on retouve en très grande fréquence le lemme "juin" puis "etat". Dans le TOPIC 2 on retrouve les lemmes "président", "guerre".

![eco-hiver-lemmes-TP1.png](./eco-hiver-lemmes-TP1.png)

A l'inverse, dans les formes nous retrouvons dans le TOPIC 1 les formes "président","guerre" puis dans le TOPIC 2 "juin" puis "mois".

On retrouve donc les mêmes termes mais répartis dans des TOPICS différents.

On retrouve dans la période "hiver" les lemmes du champ sémantique lié à la guerre : "guerre, morts, armes, lutte, victimes, civils, exil".

On a également "dirigeants, russes, sécurité,"

A partir du TOPIC 5 nous retrouvons les lemmes "avortement, soutien ", car nous avons eu au courant de l'année 2022 la question des lois contre l'avortement. C'est pourquoi au Topic 7 nous retrouvons les lemmes "enfants,femmes, loi".

Regardons les noms propres pour les lemmes nous avons le NP "ukraine" qui sans surprise est le plus fréquent. Puis "moscou", en seconde position, puis "kiev" puis "russie", les nom des pays et villes entre la Russie et l'Ukraine entremêlés ainsi, dans l'apparition des fréquences représentent bien les confrontations entre la Russie et l'Ukraine liés à l'actualité économique.

![eco-hiver-lemme-NP1.png](./eco-hiver-lemme-NP1.png)

On peut voir que le TOPIC 1 est très représenté par le lemme "ukraine", puis nous retrouvons également les lemmes les plus fréquents avec l'overall term frequency.

Lorsque l'on regarde les verbes pour les lemmes on observe que les verbes "fait, doit accusé, déclaré, pourrait, annoncé, estime, faire, devrait, mettre, quitté " sont les dans le Top 12 Most Salient terms de notre corpus. Cela nous renvoie au devoir et au vocabulaire du discours politique avec "annoncé, déclaré" mais également au registre économique avec "estime".

![eco-hiver-lemmes-verbe1.png](./eco-hiver-lemmes-verbe1.png)

Dans le Topic 1 on peut voir que les verbes "annoncé,mis,quitté" sont les plus présents.

![eco-hiver-lemme-verbe-TP1.png](./eco-hiver-lemme-verbe-TP1.png)

En observant les verbes sur leur formes afin de voir si on retrouve une différence significative on peut se rendre compte que nous retrouvons plus de verbes car nous avons ici les formes des Top 28 salient Terms cependant nous retrouvons a peu près les mêmes verbes avec "doit, fait, pourrait,annoncé,accusé", nous avons des verbes en plus comme "explique,repondu". Mais ces verbes appartiennent également au champ sémantique du discours.

![eco-hiver-formes-verb1.png](./eco-hiver-formes-verb1.png)

Dans le TOPIC 1 des formes des verbes on retrouve de nouveau les verbes "mis,quitté" comme pour les lemmes.

![eco-hiver-formes-verb-TP1.png](./eco-hiver-formes-verb-TP1.png)

Pour la saison "hiver", on constate donc que le champ sémantique politique est plus présent car on parle plus du président, des gouvernments et surtout de l'Ukraine et de la Russie. Les obligations et les verbes liés au devoir et au discours nous confirment nos analyses. 

En analysant donc tout au long de l'année la catégorie économique nous n'avons pas de grande surprise, si ce n'est peut être quelques termes sous représentés comme "carburant" qui nous montrent que la guerre en Ukraine aura été en lumière sur l'année 2022.

****CATÉGORIE INTERNATIONAL****

- **Janv à avril - Hiver**

Pour la catégorie international nous avons analysé les noms et adjectifs sur les lemmes. 

Notre premier Topic nous révèle que dans le Top-21 Most Relevant Terms for Topic 1 (17% of tokens) les lemmes les plus importants parlent de : "pays, guerre et capitale""

En analysant le Topic 2 nous avons les lemmes "monde, mort, famille"

Dans le Topic 3 on retrouve les lemmes : "etat, personne, avril"

Dans le Topic 4 on retrouve : "président, parlement"

Puis dans nos Topics suivants nous avons encore le même champ sémantique de politique et mort.

On peut voir que sur la période automne "monde" est le terme qui apparaît le plus dans tout notre corpus pour la catégorie international tout comme notre catégorie économie. N'oublions pas que nous avons le journal du même noom, "LE MONDE".

![inter-aut-lemme-1.png](./inter-aut-lemme-1.png)

Pour le TOPIC 1 des noms et adjectifs sur les lemmes.

![inter-aut-lemme-TP1.png](./inter-aut-lemme-TP1.png)

Pour les noms propres on retrouve a peu près les mêmes que pour notre catégorie économie également. Avec "Ukraine", "Russie" et "France" comme Top 5 Most Salient Terms. On peut donc conclure que aussi bien pour économie que pour international les sujets les plus présents sont la guerre entre l'Ukraine et la Russie ainsi que les sujets qui en découlent. Nous n'avons pas de surprise particulière.

![inter-aut-lemme-NP1.png](./inter-aut-lemme-NP1.png)

En ce sens, nous avons voulu analyser les verbes et là encore on peut observer des verbes de devoir et de discours avec : "prendre, pouvoir, devoir, annoncer etc."

![inter-aut-lemme-verb1.png](./inter-aut-lemme-verb1.png)

    

- **Mai à août - Été**

Lorsque l'on analyse les adjectifs + nom pour la période été sur les lemmes on observe les mêmes champs sémantiques récurrents de la guerre, du gouvernement avec "état, président, armés, guerre, etc."

![inter-ete-lemme1.png](./inter-ete-lemme1.png)

Lorsque l'on observe de plus près les TOPICS on peut voir que dans le TOPIC 1 nous avons les lemmes : "candidat, homme, exportation"

![inter-ete-lemmes-TP1.png](./inter-ete-lemmes-TP1.png)

Dans les TOPICS 7 et 8 on observe un champ sémantique particulier avec "question, élection, droit, décision", nous pouvons voir que l'on parle ici d'élection sur cette période particulière.

![inter-ete-lemmes-TP7.png](./inter-ete-lemmes-TP7.png)

![inter-ete-lemme-TP7.png](./inter-ete-lemme-TP7.png)

Pour les Noms propres des lemmes nous avons les toujours les mêmes NP que précédemment mais cependant nous pouvons en aperçevoir d'autres comme "pékin, poutine, biden, vladimir, taïwan, donbass". N'oublions pas que nous sommes ici dans la catégorie international et donc nous pouvons voir des thèmes plus larges à l'échelle mondiale. Il s'agit sûrement de questions politiques concernant ces pays.

![inter-ete-lemmes-NP1.png](./inter-ete-lemmes-NP1.png)

Pour les verbes à partir des lemmes nous pouvons voir les verbes "devoir" et "faire", une nouvelle fois des verbes d'obligation. Nous avons également "déclarer, raconter, dénoncer, menacer, dire" pour les verbes de discours. Puis "tuer, viser", nous pouvons les relier au champ sémantique de la guerre.

![inter-ete-lemmes-VB1.png](./inter-ete-lemmes-VB1.png)

- **Sept à déc - Automne**

Pour la période hiver sur la catégorie international nous avons également analysé les lemmes et dans le Top 30 Most Salient Terms nous pouvons voir que les lemmes ne changent pas énormément entre cette période et les autres, regardons de plus près dans les Next Topics.

![inter-hiver-lemme1.png](./inter-hiver-lemme1.png)

Dans le TOPIC 1 nous avons les lemmmes "mininstre", "accord", "situation", "relation", nous pouvons donc voir que le thème politique est ici assez présent.

![inter-hiver-TP1.png](./inter-hiver-TP1.png)

Dans le TOPIC 4 on remarque égalment "céréale" et "énergie", nous voyons que ces lemmes apparaissent uniquement dans ce TOPIC. Rappelons-nous qu'une pénurie de certains aliments a été au cœur de l'actualité suite à la guerre en Ukraine après les problèmes d'approvisation de GAZ pour l'Europe. 

![inter-hiver-TP4.png](./inter-hiver-TP4.png)

Pour les noms propres on observe qu'ils sont assez proches de la période été que nous avons vu précédemment. Cependant, le nom Propre "UE" apparaît dans cette période. Cela est sûrement dû aux questions économiques et politiques qui se sont étendues à plusieurs pays et soulignons que nous somme toujours dans la catégorie international.

![inter-hiver-NP.png](./inter-hiver-NP.png)

Pour les verbes nous n'avons rien de vraiment différent ou opposé aux lemmes précédents. Cela reprend ce que nous avons vu auparavant.

![inter-hiver-verb.png](./inter-hiver-verb.png)

### CLASSE POLITIQUE :

- **Janvier à Avril - Hiver**

Le graphe permet d'identifier 2 thèmes assez espacés :

Le 1er thème (topic 1) rassemble 16,45 % des tokens de l'extraction de la catégorie politique sur la période janvier à avril, 3 tokens sont particulièrement présents dont le premier avec une forte fréquence : "candidat", suivi de "droite".

![hiver_politique_NomAdj.png](./hiver_politique_NomAdj.png)

Le 2ème thème (topic 2) rassemble 10,7 % des tokens et conserve des lemmes appartenant au champ politique comme "élection, "tour", "politique", "projet, mais aussi les lemmes "chercheur" et "sociologue".

![hiver_politique_NomAdj_2.png](./hiver_politique_NomAdj_2.png)

- **Mai à août - Été**

Sur cette période on commence à voir apparaître dans le 2ème thème par la taille (seulement 10 % des tokens), les mots comme "âge", "emploi" ou encore "justice", reflétant davantage des sujets de préoccupation des citoyens. 

![ete_politique_NomAdj_2.png](./ete_politique_NomAdj_2.png)

Le thème le plus gros par la taille, estimé à 20,8 % des tokens, reste sur des mots relatifs aux élections : "tour", "circonscription", "vote", "scrutin", "voix", et au Parlement : "député", "assemblée". Concernant les tendances politique, seule la "droite" est évoquée dans ce topic. La "gauche" sera évoquée dans le thème 3.

![ete_politique_NomAdj.png](./ete_politique_NomAdj.png)

On note qu'à cette période de l'année où la sécheresse et les tensions sur l'écosystème sont fortes, seuls les mots "transition" et "écologiste" semblent rappeler cette problématique, ils apparaissent respectivement dans les thèmes 3 et 4. on peut s'attendre à ce thème soit plus présent dans la catégorie planète mais la faiblesse de ce champ sémantique dans la catégorie politique semble toutefois indiquer que l'écologie politique est peu présente.

- **Septembre à décembre - Automne**

Le mot de "réforme" apparaît dans le 1er thème 

![automne_politique_NomAdj.png](./automne_politique_NomAdj.png)

et si l'on regarde les verbes c'est le verbe "annoncer" qui domine par sa fréquence estimée. La rentrée a vu le début des annonces de réformes.

![automne_politique_Verb.png](./automne_politique_Verb.png)

- Sur l'année 2022 

On note la présence forte des tokens "gouvernement" et "chef", suivis de "assemblée". "exécutif"" vient après.

On note la fréquence de 2 mois : "juin" et "décembre" à relier au calendrier politique, votes de l'assemblée, etc.

![annee_politique_NomAdj.png](./annee_politique_NomAdj.png)

On note aussi s'agissant des verbes que "ouvrir", "recevoir", "concerner", "promettre" sont parmi les plus présents, juste avant "refuser".

![annee_politique_verb.png](./annee_politique_verb.png])

# Conclusion

À travers notre corpus nous avons pu étudier l’actualité depuis quatre points de vue différents et à travers 3 fils conducteurs : l’automne, l’été et l’hiver.

Si pour chaque thème des spécificités dues à la classe étudiée se sont fait ressentir nous avons également vu des points communs notamment comme le lemme « monde » que nous avons retrouvé au fil de tout notre corpus. C’est pourquoi nous devons analyser avec parcimonie nos classes, car certains termes ne sont pas révélateurs de données particulières ou ne révèlent pas des spécificités au sein du corpus. Nous avons vu que les thèmes récurrents étaient comme nous pouvions le supposer la guerre en Ukraine, les débats politiques et le réchauffement climatique au cours de l’année 2022.

Cependant, nous avons pu constater parfois certain thèmes mis au jour par notre corpus comme l’incendie de l’apiculteur à Landiras, commune française ou bien le sujet de l’avortement et des combats féministes. Cela reste important à souligner car si certains sujets sont bien présents dans nos mémoire encore actuellement, avec le temps ils peuvent tendre à s’effacer face à d’autres faits plus actuels. Il nous a semblé très important d’avoir accès à cette pipeline d’informations afin d’étudier sur la durée des données informatives qui souvent ne peuvent pas être analysées de manière isolé. Cette étude n’est pas exhaustive mais nous a permis de nous rendre compte des enjeux de la chaine d’information et de la richesse d’un flux RSS.
