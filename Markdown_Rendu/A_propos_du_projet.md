**A propos du projet:**

Dans le cadre du cours de Programmation et Projet Encadré du semestre 2 du master TAL, nous nous sommes intéressées à l'analyse des flux RSS.
Ces données très intéressantes permettent d'analyser les sujets de l'actualité, les tendances et évènements qui ressortent pendant une certaine période.
Ainsi, grâce au flux RSS, on peut s'intéresser à un sujet précis et savoir ce qu'il en ressort, quels débats il pose et quels sont les grand faits mis en lumière par les médias.
Nous nous sommes plus particulièrement intéresser à la question de l'environnement : quels en sont les enjeux, les impacts politiques économiques et sociaux et quels faits environnementaux ont marqué l'année 2022 ?
Pour répondre à cette question, nous allons analyser un corpus de flux RSS pris sur des médias sur toute l'année 2022 fourni en cours de PPE.
Ce corpus est organisé par date et par catégories. Celles qui seront plus particulièrement intéressantes pour notre sujet seront planete, économie, international et politique.
En effet, nous allons chercher à analyser quels liens peut-on trouver entre ces catégories ce qui pourra nous révéler l'impact d'une catégorie sur l'autre et nous nous poserons notamment les questions suivantes :
- comment la question de l'environnement est perçue à l'international ?
- Comment l'environnement ressort dans la sphère politique ? Est-ce un sujet majeur ou au contraire qui ressort peu ?
- L'environnement a t-il des impacts sur l'économie ?

**Hypothèses:**<br>
Avant de regarder les données, nous pouvons déjà supposer que certains thèmes ressortiront particulièrement en raison de grand évènements en 2022.
Nous pouvons notamment penser aux incendies qui sont une constante problématique en été et la question du gaz en hiver notamment avec la guerre en ukraine.


**A propos de nous:**<br>
Notre projet surnommé Aldies a été possible grâce à la rencontre de trois étudiantes grâce au cours de PPE. Nous sommes Alice Wallard étudiante à l'Inalco, Elodie Esteves et Anne-Sophie Foussat étudiantes à la Sorbonne Nouvelle.
