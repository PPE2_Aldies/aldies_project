#### Arborescence des répertoires de données

```mermaid
graph TB;  
    A("Corpus")--> B("2022")
    B("2022")--> C("Jan")
    B("2022")--> D("Feb")
    B("2022")--> E("Mar")
    B("2022")--> F("Avr")
    B("2022")--> G("May")
    B("2022")--> H("Jun")
    B("2022")--> I("Jul")
    B("2022")--> J("Aug")
    B("2022")--> K("Sep")
    B("2022")--> L("Oct")
    B("2022")--> M("Nov")
    B("2022")--> N("Dec")
classDef vert1 fill:#0ef7c9,stroke:#333,stroke-width:2px
classDef vert2 fill:#3cf70e,stroke:#333,stroke-width:2px
classDef vertclair fill :#b0f70e, stroke:#333,stroke-width:2px
class A vert1
class B vert2
class C,D,E,F,G,H,I,J,K,L,M,N vertclair
```

#### Exemple pour un mois donné

```mermaid
graph TB;
    A("Apr")-->|jour du mois|B("01")
    A("Apr")--> C("02")
    A("Apr")--> D("03")
    A("Apr")--> E("...")
    A("Apr")--> F("31")
    B("01")-->|répertoire des fichiers|G("19-00-00")
    G("19-00-00")-->|fichier .txt|H["0,2-3208,1-0,0.txt"]
    G("19-00-00")-->|fichier .xml|I["0,2-3208,1-0,0.xml"]
classDef vertclair fill :#b0f70e, stroke:#333,stroke-width:2px
classDef vertclair2 fill :#d7fb87, stroke:#333,stroke-width:2px
classDef vertclair3 fill :#ebfdc3, stroke:#333,stroke-width:2px
classDef jaune fill :#ffe599, stroke:#333,stroke-width:2px
class A vertclair
class B,C,D,E,F vertclair2
class G vertclair3
class H,I jaune
```

#### Parcours de l'arborescence

```mermaid
flowchart TD
 A1("En jaune le parcours qui nous intéresse dans le fichier.xml") -.- A
 A("RSS")--> B("channel")
 B("channel")--> C("title")
 B("channel")--> D("description")
 B("channel")--> E("copyrigth")
 B("channel")--> F("link")
 B("channel")--> G("pubDate")
 B("channel")--> H("atom")
 B("channel")--> I("item")
 I("item")--> J("title")
 I("item")--> K("pubDate")
 I("item")--> L("description")
 I("item")--> M("guide")
 I("item")--> N("link")
 I("item")--> O("media:content")
 O("media:content") --> P("media:description")
 O("media:content") --> Q("media:credit")
classDef col1 fill:#fbf653,stroke:#333,stroke-width:2px
classDef col2 fill:#f58fbd,stroke:#333,stroke-width:2px
classDef col3 fill :#f5948f, stroke:#333,stroke-width:2px
classDef col4 fill :#f5c78f, stroke:#333,stroke-width:2px
classDef col5 fill :#f0f58f, stroke:#333,stroke-width:2px
class A1,A2,A,B,I,J,L col1
class C,D,E,F,G,H,K,M,N,O,P,Q col3
```

#### Lancer le script et choix de parseur, étiquetteur et format

Les analyses ont été automatisé grâce à des scripts python et bash.  
Le script script_main.sh permet de choisir une date de début et une date de fin ainsi que une catégorie. Optionnellement, il est également possible de choisir
l’étiquette de l’analyse : lemme ou forme mais lemme sera
choisi par défaut.

En ce qui concerne les formats et paramètres d’analyse, nous avons choisi d’utiliser la méthode eTree que nous avons trouvé idéale pour parser des documents xml.
Comme notre corpus est sous format xml, cela semblait être la
méthode la plus naturelle et la plus simple. Nos autres fonctions
sont disponibles dans notre main sur github dans le dossier
dernier/extract_many_ok.

Pour l’analyser morphosyntaxiques, nous avons choisi stanza qui est un étiquetteur très performant. 

En effet, selon CoNLL stanza ferait 10 % moins d’erreur que
spacy. Aussi, son installation et utilisation était très simple
tandis que trankit posait des soucis de compatibilités avec la
version python d’un ordinateur d’un membre de notre équipe
Aldies.

Enfin, pour le format de sauvegarde des données enrichies, nous avons choisi json qui stocke les données sous forme de dictionnaire. Ce format nous a semblé très pratique à analyser avec python qui permet de parcourir les données d’un dictionnaire très facilement. La fonction json est également très simple et ne demande que peu de lignes de code.

Bien que nous ayons fait des choix pour le script script_main.sh, en lançant les programmes séparemment, tous ces possibilités peuvent être sélectionnées en argument en ligne de commande. Les fonctions sont disponnibles dans le repértoire
github. 

Il nous a toutefois semblé plus simple de limiter le choix
des arguments avec le script main car certaines fonctions nous
semblent plus performante et trop de choix d’arguments pourraient
compliquer la ligne de commande.



#### Les scripts

**a) extract_many_ok.py**

Ce script utilise les fonctions xml.etree, re, extract_un_fil, export_json, export_xml,
export_pickle, analyse_trankit, analyse_spacy, et analyse_stanza qui
ont été créé au préalable pour pouvoir effectuer des choix dans
la méthode d’analyse.

Pour pouvoir naviguer dans le corpus, les codes des catégories sont enregistrés
par catégorie, ont enregistre également les dates pour pouvoir
choisir les périodes qui nous intéressent.

Tout d’abord, l’utilisateur peut choisir la méthode de parsing désirée.

Il a le choix entre la méthode etree qui permet de parser les documents
xml, la méthode re qui permet de rechercher les balises grâce à
des expressions régulières et feedparser qui parcours le fichier
xml comme s’il s’agissait d’un dictionnaire.

Le programme lance la fonction de la méthode choisie et retourne le résultat dans la
variable func. Le même choix se fait pour l’étiquetteur
permettant de choisir entre stanza, spacy et trankit.

Ensuite, en fonction des dates et des catégories, on récupère le corpus en le
parsant et l’analysant et on le stocke dans le format choisi (json,
xml ou pickle).

**b) gensim_MAJ.py**

Il s’agit du script qui va permettre de visualiser ce qui ressort de ce corpus en
fonction d’une catégorie. Pour cela, nous allons utiliser le
module gensim qui va permettre une vectorisation des mots du corpus
en fonction de leur fréquence.

Pour lancer le script, il faut préciser quel format a été utilisé dans l’étape
précédante pour pouvoir récupérer l’étiquette qui nous
intéresse en fonction du type de format.

En effet, puisque les formats sont différents, le stockage des informations n’est
pas le même et il a donc fallu adapter le parcour des données par
des fonctions LdaModel_pickle, LdaModel_json et LdaModel_xml.

Quelque soit le format, ces fonctions vont retourner une liste stockée dans
la variable étiquette qui contient une liste de lemmes,
une liste de formes et une liste de pos.

En fonction du pos choisie, le programme va utiliser la fonction get_pos
pour trier les étiquettes et ne garder que les formes et les lemmes
qui correspond au pos choisi. Ensuite, le programme va garder
uniquement la partie de l’étiquette qui à été choisie en
argument : soit les formes soit les lemmes avant de lancer la
vectorisation avec gensim.

Pour la vectorisation, le programme met les mots en minuscules, supprime
les chiffres qui ne sont pas dans des mots et supprime les chaines de
un seul caractère. Après supprimer les mots trop fréquents ou trop
peu fréquents grâce à des paramètres, le programme effectue une vectorisation et fait
apparaître les termes les plus fréquents par topics. Les paramètres pouvant être choisi
en ligne de commande permettre de régler la fréquence minimum et maximum des mots dans
le corpus, par défaut le programme est lancé avec les paramètres par défauts 20 et 0.5 
(mots apparaissant dans au moins 20 documents et pas dans plus de 50% des documents).
Ensuite, le programme effectue une visualisation de ce résultat dans le fichier
visualisation.html.
