#!/bin/bash
mkdir -p Corpus_r2
for FIC in $(ls ./Corpus/*.txt)
do
	TEXT=$(cat $FIC | tr '\n' ' ')
	echo "$TEXT" > "./Corpus_r2/$(basename $FIC )"
done