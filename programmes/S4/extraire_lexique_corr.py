#!usr/bin/env python
#-*- coding: utf-8 -*-
from typing import List, Dict
import errno
from pathlib import Path
import argparse
import sys
import os


""" But de ce pgm : utiliser argparse afin de permettre au pgm d'avoir plusieurs 
types d'interactions avec le Terminal et à l'utilisateur de pouvoir sélectionner 
ces interactions. """

def lire_corpus_r1(fichiers: List[str])->List[str]:
	"""
	execution dans Bash: 
	$ python extraire_lexique.py Corpus/*.txt
	Lit le corpus comme une liste de fichiers en arguments:
	renvoie une liste où chaque élément est le contenu d'un fichier sous forme de string
	"""
	resultat=[]
	for fichier in fichiers:
		texte=Path(fichier).read_text("utf-8")
		resultat.append(texte)
	return resultat

def lire_corpus_r2()->List[str]:
	"""
	execution dans Bash: 
	$ cat Corpus/*.txt | python extraire_lexique.py
	Crée un Corpus_r2 de fichiers nettoyé des passages à la ligne.
	Affiche le contenu des fichiers du Corpus sur l'entrée standard, un fichier par ligne.
	Lit ce corpus depuis l'entrée standard.
	Renvoie une liste où chaque élément est le contenu d'un fichier sous forme de string
	"""
	# Appel au script bash de nettoyage et création Corpus_r2
	os.system("bash convert_corpus.sh")
	resultat=[]
	for texte in sys.stdin:
		resultat.append(texte)
	return resultat
	
def lire_corpus_r3()->List[str]:
	"""
	execution dans Bash: 
	$ ls Corpus/*.txt | python extraire_lexique.py
	Lit le contenu des fichiers du Corpus sur l'entrée standard et le redirige vers le pgm
	"""
	resultat=[]
	for fichier in sys.stdin:
		texte = Path(fichier.strip()).read_text("utf-8")
		resultat.append(texte)
	return resultat
	
def term_freq(liste_texte:List[str])->Dict[str,int]:
	"""
	Lit les éléments de la liste qui sont des contenus de fichiers texte.
	Retourne un dictionnaire associant chaque mot à son nombre d'occurrences dans le corpus.
	"""
	term_freq_dic={}
	for texte in liste_texte:
		for word in texte.split():
			if word in resultat:
				term_freq_dic[word]+=1
			else:
				term_freq_dic[word]=1
	return term_freq_dic

def texte_freq(liste_texte:List[str])->Dict[str,int]:
	"""
	Lit les éléments de la liste qui sont des contenus de fichiers texte.
	Retournant un dictionnaire associant chacun des mots au nombre de documents dans lesquels il apparaît.
	"""
	def doc_freq(corpus: List[str]) -> Dict[str,int]:
		resultat = {}
		for doc in corpus:
			words = set(doc.split())
			for word in words:
				if word in resultat:
					resultat[word] += 1
				else:
					resultat[word] = 1
	return resultat
	
def main():
	"""
	argument positionel de r1: Corpus/*.txt
	Récupère les arguments quand il y en a avec argparse
	on utilise nargs pour créer une List[str] (str : nom d'un fichier)
	Teste pour faire un choix des solutions de lecture (r1, r2 ou r3)
	selon ce qu'il y a à la ligne de commande
	"""
	# création d'un objet argparse
	parser = argparse.ArgumentParser(prog="extraire_lexique.py", usage="%(prog)s [-r1, -r2, -r3], see below", \
																description="L'utilisateur peut utiliser les options -r1 ou -r2 ou -r3 pour lire le corpus")
	# méthode add_argument() pour spécifier les arguments valables
	parser.add_argument("fichiers", help="à la ligne de commande :  python %(prog)s Corpus/*.txt", nargs="*", \
											default="Entrez une option, voir help")
	# Ajout des argument optionnels pour sélectionner la fonction de lecture
	parser.add_argument("-r1", action="store_true", help="à la ligne de commande : python %(prog)s Corpus/*.txt \
											Lit les fichiers donnés en argument.", default="Entrez une option, voir help")
	parser.add_argument("-r2", action="store_true", help="à la ligne de commande : cat Corpus/*.txt | python %(prog)s \
											Crée un Corpus_r2 nettoyé des passages à la ligne. Lit le contenu des fichiers affichés sur sdin.", \
											default="Entrez une option, voir help")
	parser.add_argument("-r3", action="store_true", help="à la ligne de commande : ls Corpus/*.txt | python %(prog)s\
											Lit les fichiers listés sur stdin.", default="Entrez une option, voir help")
	# méthode parse_args()
	# lance le segmenteur et renvoie les données extraites en fonction des arguments et options lors de son appel
	args = parser.parse_args()
	# Appel de la méthode args sur l'argument, va retourner une List car nargs="*"
	fichiers = args.fichiers
	
	# 
	if args.r1 and len(args.fichiers)>0:
		corpus = lire_corpus_r1(args.fichiers)
		print(corpus)
	
	elif args.r3:
		corpus = lire_corpus_r3()
		print(corpus)
	
	else:
		corpus = lire_corpus_r2()
		print(corpus)
		
if __name__ == "__main__":
	try:
		main()
	except IOError as e:
		if e.errno == errno.EPIPE:
			pass
	Path("/tmp/plop").touch()