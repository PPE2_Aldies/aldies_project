#principal script sh qui lance les autres scripts nécessaires à l'analyse

if [[ $# < 3 ]]
then
    echo "3 arguments obligatoires attendus : start_date, end_date et catégorie exemple 2022-01-01 et 2022-04-30 planete"
    exit 
fi


#date de debut format annee-mois-jour
start_date=$1
#date de fin
end_date=$2
#categorie, défaut = planete
cat=$3

#etiquette : lemme ou pos
etiquette=${7:-lemme}

#etape 1 extraction du corpus sous un format
python dernier/extract_many_ok/extract_many_ok.py -m et -a stanza -f json -s $start_date -e $end_date Corpus_RSS/2022 $cat -o sortie
#etape 2 vectorisation et visualisation gensim
python dernier/extract_many_ok/gensim_MAJ.py -f sortie.js -p noun+adj -d $etiquette