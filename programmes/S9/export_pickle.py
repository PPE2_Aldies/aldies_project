import pickle
from datastructures import Corpus

def write_pickle(corpus: Corpus, destination: str):
	with open(destination, "wb") as fout:
	    pickle.dump(corpus, fout)
	    return fout

def read_pickle(fichier_dump):
	print(pickle.load(open(fichier_dump, 'rb')))
	return