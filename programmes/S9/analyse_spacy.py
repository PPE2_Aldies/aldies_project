import spacy
from datastructures import Article, Token

def analyse_spacy(article):
	nlp = spacy.load("fr_core_news_sm")
	desc = article.description
	p = nlp(desc)
	list_analyse = []
	for t in p:
		token = Token(t.text, t.lemma_, t.pos_)
		list_analyse.append(token)
	article = Article(article.titre, desc, list_analyse)
	return article