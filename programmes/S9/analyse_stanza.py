#!/usr/bin/env python
#-*- coding: utf-8 -*-
import stanza
import json
from datastructures import Article, Token

def analyse_stanza(article):
	desc = article.description
	list_analyse = []
	#for e in desc:
	nlp = stanza.Pipeline(lang='fr', processors='tokenize, mwt, pos, lemma')
	doc = nlp(desc)
	#liste = [f'{word.text+" "}, \t{word.lemma}\tpos: {word.pos}' for sent in doc.sentences for word in sent.words]
	for sent in doc.sentences:
		for word in sent.words:
			token = Token(word.text, word.lemma, word.pos)
			list_analyse.append(token)
	article = Article(article.titre, desc, list_analyse)
	return article

def sortie_json(dico):
	sortie = json.dumps(dico)
	return sortie