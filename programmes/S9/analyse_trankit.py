import trankit
from datastructures import Article, Token

def analyse_trankit(article):
	p = trankit.Pipeline("french")
	desc = article.description
	dico = p(desc)
	# accès à 'tokens' dans le dico
	liste = dico.get('sentences')[0].get('tokens')
	list_analyse = []
	for d in liste:
		# on remplit les objets Token avec les valeurs
		token = Token(d.get('text'), d.get('lemma'), d.get('upos'))
		list_analyse.append(token)
	article = Article(article.titre, desc, list_analyse)
	return article