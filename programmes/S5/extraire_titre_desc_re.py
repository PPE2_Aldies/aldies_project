#!usr/bin/env python
#-*- coding: utf-8 -*-

from typing import Iterable, List
import re
from pathlib import Path
import argparse

def extract_title_desc(string_xml: str) -> List[tuple[str]]:
	"""
	Normalise les espaces insécables et trouve les titres et les descriptions
	les renvoie sous forme de tuple(titre, description)
	"""
	#normalisation des espaces insécables
	string_xml = re.sub('\\xa0»', '"', string_xml)
	string_xml = re.sub('«\\xa0', '"', string_xml)
	string_xml = re.sub('\\xa0:', ' :', string_xml)
	match_titre_desc = re.findall("<item><title><!\[CDATA\[(.*?)\]\]><\/title>.*?<\/pubDate><description><!\[CDATA\[(.*?)\]\]><\/description>", string_xml)
	for e in match_titre_desc:
		print(f"Titre : {e[0]}\nDescription : {e[1]}\n")
	return match_titre_desc

def main():
	# création d'un objet argparse
	parser = argparse.ArgumentParser()
	# méthode add_argument() pour spécifier les arguments valables
	parser.add_argument("fichier", help="nom du fichier.xml")
	# méthode parse_args()
	# lance le segmenteur et renvoie les données extraites en fonction des arguments et options lors de son appel
	args = parser.parse_args()
	# Appel de la méthode args sur l'argument fichier, renvoie le nom du fichier
	fichier = args.fichier
	texte = Path(fichier).read_text("utf-8")
	return extract_title_desc(texte)


if __name__ == "__main__":
	#try:
		main()
	# except IOError as e:
# 		if e.errno == errno.EPIPE:
# 			pass
# 	Path("/tmp/plop").touch()