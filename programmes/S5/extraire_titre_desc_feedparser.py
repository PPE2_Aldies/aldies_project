#!usr/bin/env python
#-*- coding: utf-8 -*-

from typing import Iterable, List, Dict
import argparse
import feedparser

def extract_title_desc(feed: Dict[str, List[Dict[str,str]]]) -> Dict[str,str]:
	"""
	Trouve les titres et les descriptions
	les renvoie sous forme de dictionnaire clé : titre, valeur : description.
	"""
	# list(feed.items())[1] correspond à 'entries'
	# list(feed.items())[1][1] correspond à la valeur de 'entries', List[Dict[str,str]]] :
	# Liste de dicos de 14 entrées, dont : 'title' (clé 1) et 'summary' (clé 5)
	# chaque dico est un fill RSS
	liste = list(feed.items())[1][1]
	titre_desc_dico={}
	for dico in liste:
		titre_text=dico['title']
		desc_text=dico['summary']
		print(f"Titre : {titre_text}")
		print(f"Description : {desc_text}\n")
		titre_desc_dico[titre_text]=desc_text
	#print(titre_desc_dico)
	return titre_desc_dico
	

def main():
	# création d'un objet argparse
	parser = argparse.ArgumentParser()
	# méthode add_argument() pour spécifier les arguments valables
	parser.add_argument("fichier", help="nom du fichier.xml")
	# méthode parse_args()
	# lance le segmenteur et renvoie les données extraites en fonction des arguments et options lors de son appel
	args = parser.parse_args()
	# Appel de la méthode args sur l'argument fichier, renvoie le nom du fichier
	fichier = args.fichier
	feed = feedparser.parse(fichier)
	return extract_title_desc(feed)

if __name__ == "__main__":
	#try:
		main()
	# except IOError as e:
# 		if e.errno == errno.EPIPE:
# 			pass
# 	Path("/tmp/plop").touch()