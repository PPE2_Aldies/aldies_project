#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 15 12:55:49 2023

@author: ilodia
"""

import argparse
import xml.etree.ElementTree as ET

def extract_title_desc(fichier_traite):
	tree = ET.parse(fichier_traite)
	root = tree.getroot()
	compteur =1
	for element in root.iter('item'):
		for child in element:
			if child.tag=='title':
				print(compteur, ' :','Titre : ',child.text)
			if child.tag=='description':
				print(compteur, ' :','Desc : ',child.text)
		compteur+=1
	return
	
def main():
	# activation argparse
	argParser = argparse.ArgumentParser()
	# Création argument obligatoire : nom du fichier XML à traiter
	argParser.add_argument('filename', help="XML file.xml")
	args = argParser.parse_args()
	fichier_traite = args.filename
	return extract_title_desc(fichier_traite)
	
if __name__ == "__main__":
	main()