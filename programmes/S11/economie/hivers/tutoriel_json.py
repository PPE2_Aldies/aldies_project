import argparse
import sys
import pyLDAvis
import pyLDAvis.gensim_models as gensimvis
import xml.etree.ElementTree as ET
from typing import List

def LdaModel_etiquette(liste):
    docs = liste
    for idx in range(len(docs)):
        docs[idx] = docs[idx].lower()  # Convert to lowercase.
        docs[idx]=docs[idx].split(' ')

    # Remove numbers, but not words that contain numbers.
    docs = [[token for token in doc if not token.isnumeric()] for doc in docs]
    # Remove words that are only one character.
    docs = [[token for token in doc if len(token) > 1] for doc in docs]
    
    # Compute bigrams.
    from gensim.models import Phrases
    #recuperer n gramme/ bigramme pour récupérer expressions figées possible sur gensim d'ou les add bigram pour recuperer que ceux qui sont interessant
    # Add bigrams and trigrams to docs (only ones that appear 20 times or more).
    bigram = Phrases(docs, min_count=20)
    for idx in range(len(docs)):
        for token in bigram[docs[idx]]:
            if '_' in token:
                # Token is a bigram, add to document.
                docs[idx].append(token)

    # Remove rare and common tokens.
    from gensim.corpora import Dictionary

    # Create a dictionary representation of the documents.
    dictionary = Dictionary(docs)

    # Filter out words that occur less than 20 documents, or more than 50% of the documents.
    dictionary.filter_extremes(no_below=20, no_above=0.5)

    # Bag-of-words representation of the documents.
    corpus = [dictionary.doc2bow(doc) for doc in docs]

    print('Number of unique tokens: %d' % len(dictionary))
    print('Number of documents: %d' % len(corpus))

    # Train LDA model.
    from gensim.models import LdaModel

    # Set training parameters.
    num_topics = 10
    chunksize = 2000
    passes = 20
    iterations = 400
    eval_every = None  # Don't evaluate model perplexity, takes too much time.

    # Make an index to word dictionary.
    temp = dictionary[0]  # This is only to "load" the dictionary.
    id2word = dictionary.id2token

    model = LdaModel(
        corpus=corpus,
        id2word=id2word,
        chunksize=chunksize,
        alpha='auto',
        eta='auto',
        iterations=iterations,
        num_topics=num_topics,
        passes=passes,
        eval_every=eval_every
    )

    top_topics = model.top_topics(corpus)

    # Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
    avg_topic_coherence = sum([t[1] for t in top_topics]) / num_topics
    print('Average topic coherence: %.4f.' % avg_topic_coherence)

    from pprint import pprint
    pprint(top_topics)
    vis_data = gensimvis.prepare(model, corpus, dictionary)
    with open('visualisation.html', "w") as f:
        pyLDAvis.save_html(vis_data, f)

def LdaModel_xml(etiquettes) -> List:
    # Parsing du fichier XML
    tree = ET.parse("corpus_xml.xml")
    root = tree.getroot()
    liste_lemmes = []
    liste_formes = []
    liste_pos = []
    # Parcours des éléments "article" dans le fichier XML
    articles = root.findall("content/article")
    for article in articles:
        for e in article:
            forme = e.text.strip()
            liste_formes.append(forme)

    for article in articles:
        for el in article:
            if len(el) > 1:
                lemme = el.find("lemme").text
                print(lemme)
                forme = el.find("forme").text
                pos = el.find("pos").text
                if lemme != ',' and lemme != '.' and lemme != "»":
                    liste_lemmes.append(lemme)
                    liste_formes.append(forme)
                    liste_pos.append(pos)
    print(liste_lemmes)
    print(liste_formes)
    print(liste_pos)
    # Ajout des listes d'étiquettes à la liste principale
    etiquettes.append(liste_formes)
    etiquettes.append(liste_lemmes)
    etiquettes.append(liste_pos)
    return etiquettes

def LdaModel_json(etiquettes):
    import json
    # Opening JSON file
    f = open('corpus_json.js')
    # returns JSON object as 
    # a dictionary
    data = json.load(f)
    liste_lemmes = []
    liste_formes = []
    liste_pos = []
    jsonData = data["articles"]
    #nous allons récupérer dans les clés articles les valeurs de formes et de lemmes et les ajouter aux listes
    #Remarque : la liste_lemmes n'est pas utile si on lemmatise les lemmes avec wordnet
    for x in jsonData:
        keys = x.keys()
        values = x.values()
        for e in values:
            for el in e:
                if len(el) > 1:
                    lemme = el["lemme"]
                    forme = el["forme"]
                    pos = el["pos"]
                    if lemme != ',' and lemme != '.' and lemme != "»":
                        liste_lemmes.append(lemme)
                        liste_formes.append(forme)
                        liste_pos.append(pos)
    #recuperer les pos pour chaque token et les ajouter aux attributs que si noms propres
    etiquettes.append(liste_formes)
    etiquettes.append(liste_lemmes)
    etiquettes.append(liste_pos)
    return etiquettes

def get_pos(etiquette, p):
	tri_formes = []
	tri_lemmes = []
	for i in range(len(etiquette[2])):
			pos = etiquette[2]
			formes = etiquette[0]
			lemmes= etiquette[1]
			if pos[i] == p:
				tri_formes.append(formes[i]) 
				tri_lemmes.append(lemmes[i])
	return tri_formes, tri_lemmes



if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-f", help="type de format (json, xml ou pickle)", default="json")
	parser.add_argument("-d", help="type d etiquette (lemme ou forme)", default="forme")	
	parser.add_argument("-p", help="pos choisi ('tout', 'noun', 'verb', 'noun+adj', 'adj')", default="tout")
	parser.add_argument("-o", default=None, help="génère la visualisation ldaviz et la sauvegarde dans le fichier html indiqué")
	args = parser.parse_args()
	etiquettes = []
	if args.f == 'json':
		etiquette = LdaModel_json(etiquettes)
		if args.p == 'tout':
			etiquette == etiquette[0:1]
		elif args.p == 'noun':
			etiquette = get_pos(etiquette, 'NOUN')
		elif args.p == 'verb':
			print(etiquette)
			etiquette = get_pos(etiquette, 'VERB')
		elif args.p == 'adj':
			etiquette = get_pos(etiquette, 'ADJ')
		elif args.p == 'propn':
			etiquette = get_pos(etiquette, 'PROPN')
		elif args.p == 'noun+adj':
			etiquette = get_pos(etiquette, 'NOUN') + get_pos(etiquette, 'ADJ')
	elif args.f == 'xml':
		etiquette = LdaModel_xml(etiquettes)
	print(etiquette)
	if args.d == 'forme':
		func = LdaModel_etiquette(etiquette[0])
	elif args.d == 'lemme':
		func = LdaModel_etiquette(etiquette[1])
    