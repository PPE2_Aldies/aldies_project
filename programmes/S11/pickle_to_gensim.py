from typing import List, Dict
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def pickle_load(fichier_pickle):
	from export_pickle import read_pickle
	texte = read_pickle(fichier_pickle)
	liste_articles=[]
	for a in texte.articles:
		article = a.titre+" "+a.description
		liste_articles.append(article)
	return liste_articles

def tokenize(liste_articles:List[str]) -> List[str]:
	from nltk.tokenize import RegexpTokenizer
	tokenizer = RegexpTokenizer(r'\w+')
	for idx in range(len(liste_articles)):
		liste_articles[idx] = liste_articles[idx].lower()  # Convert to lowercase.
		liste_articles[idx] = tokenizer.tokenize(liste_articles[idx])  # Split into words.
	# Remove numbers, but not words that contain numbers.
	docs = [[token for token in doc if not token.isnumeric()] for doc in liste_articles]
	# Remove words that are only one character.
	tokenized_docs = [[token for token in doc if len(token) > 1] for doc in liste_articles]
	return tokenized_docs

def lemmatisation(tokenized_docs:List[List[str]]) -> List[List[str]]:
	from nltk.stem.wordnet import WordNetLemmatizer
	lemmatizer = WordNetLemmatizer()
	lemmatized_docs = [[lemmatizer.lemmatize(token) for token in doc] for doc in tokenized_docs]
	return lemmatized_docs
	
def remove_stopwords(lemmatized_docs:List[List[str]]) -> List[List[str]]:
	with open("stopwords.txt", 'r') as f:
		stopfile = f.read()
	for a in lemmatized_docs:
		for i,t in enumerate(a):
			if t in stopfile:
				a.pop(i)
	docs_sans_stopwords = lemmatized_docs
	return docs_sans_stopwords

def compute_ngrams(docs_sans_stopwords:List[List[str]]) -> List[List[str]]:
	from gensim.models import Phrases
	# Add bigrams and trigrams to docs (only ones that appear 20 times or more).
	bigram = Phrases(docs_sans_stopwords, min_count=20)
	for idx in range(len(docs_sans_stopwords)):
		for token in bigram[docs_sans_stopwords[idx]]:
			if '_' in token:
				# Token is a bigram, add to document.
				docs_sans_stopwords[idx].append(token)
	docs_ngram = docs_sans_stopwords
	return docs_ngram

def remove_rare_common(docs_ngram:List[List[str]], no_below:int, no_above:int) -> Dict:
	from gensim.corpora import Dictionary
	# Create a dictionary representation of the documents.
	dictionary = Dictionary(docs_ngram)
	# Filter out words that occur in less than 10 documents, or more than 50% of the documents.
	dictionary.filter_extremes(no_below=no_below, no_above=no_above)
	return dictionary
	
def bag_of_words(dictionary:Dict) -> List[List[tuple[int]]]:
	corpus_int = [dictionary.doc2bow(doc) for doc in docs_ngram]
	return corpus_int
	
def train_LDA(dictionary: Dict, corpus_int: List[List[tuple[int]]], num_topics:int, chunksize:int, passes:int, iterations:int, eval_every:int):
	from gensim.models import LdaModel
	#eval_every = None  # Don't evaluate model perplexity, takes too much time.

	# Make an index to word dictionary.
	temp = dictionary[0]  # This is only to "load" the dictionary.
	id2word = dictionary.id2token

	model = LdaModel(
		corpus=corpus_int,
		id2word=id2word,
		chunksize=chunksize,
		alpha='auto',
		eta='auto',
		iterations=iterations,
		num_topics=num_topics,
		passes=passes,
		eval_every=eval_every
	)
	return model
	
def top_topics(model, corpus, num_topics:int):
	top_topics = model.top_topics(corpus)
	# Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
	avg_topic_coherence = sum([t[1] for t in top_topics]) / num_topics
	print('Average topic coherence: %.4f.' % avg_topic_coherence)
	from pprint import pprint
	pprint(top_topics)
	return top_topics
	
if __name__ == "__main__":
	liste_articles = pickle_load("sortie_pickle")
	tokenized_docs = tokenize(liste_articles)
	lemmatized_docs = lemmatisation(tokenized_docs)
	docs_sans_stopwords = remove_stopwords(lemmatized_docs)
	docs_ngram = compute_ngrams(docs_sans_stopwords)
	dictionnaire = remove_rare_common(docs_ngram, 10, 0.5)
	corpus_int = bag_of_words(dictionnaire)
	model = train_LDA(dictionnaire, corpus_int, 8, 180, 20, 300, None)
	top_topics = top_topics(model, corpus_int, 8)
	