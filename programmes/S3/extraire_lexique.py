# !/usr/bin/env python
# coding: utf-8

# Fonction r1 : 
def liste_de_chaine(rep):
    """
    Fonction qui construit une liste de chaînes où chaque chaîne
    correspond au contenu texte d’un fichier
    On utilise le module Path pour itérer sur les fichiers texte du répertoire
    param : un répertoire de fichiers
    return : une liste dont chaque élément est le contenu d'un fichier 	List[str]
    """
    liste_chaine = []
    files = Path(rep).glob('*.txt')
    for infile in files:
        ouverture = open(infile, 'r')
        chaine = ouverture.read()
        liste_chaine.append(chaine)
        return liste_chaine

# Fonction r2
def dico_liste(liste):
	"""
	Fonction prenant comme argument une liste de chaînes et 
	retournant un dictionnaire associant chaque mot à son nombre d’occurrences dans le corpus.
	param : liste de chaînes de caractères
	return : dictionnaire dic[Str]
	"""
	dico = {}
	for chaine in liste :
		chaine = chaine.strip()
		chaine = chaine.lower()
		mots = chaine.split()

# On crée une boucle for afin de rechercher le mot est déjà présent dans le dictionnaire
		for mot in mots :
			if mot in dico :
				dico[mot] = dico[mot] +1 #Si il est présent on augmente sa valeur de 1
			else :
				dico[mot] = 1 #sinon on reste à 1
	return dico

# Fonction r3 :
def dico_from_file(liste):
	"""
	Fonction prenant comme argument une liste de chaînes où chaque chaîne = un texte issu d'un document, 
	et retournant un dictionnaire associant chacun des mots de toutes les chaînes au nombre de documents 
	donc d'éléments dans lesquels il apparaît.
	param : liste de chaînes de caractères (str)
	return : dictionnaire mot -> nbre d'éléments de la liste dans lesquels le mot apparaît
	"""
	freq_mot_texte=defaultdict(int)
	set_words=set(' '.join(liste).split())
	for word in set_words:
		for i in range(0, len(liste)):
			if word in liste[i]:
				freq_mot_texte[word]+=1
	return freq_mot_texte
	
if __name__ == "__main__":
	
	from pathlib import Path
	from collections import defaultdict
	from glob import glob
	import pprint as pp
	
	# Fonction r1
	nom_rep='Corpus'
	liste=liste_de_chaine(nom_rep)
	pp.pprint(f"Liste de textes : {liste[0:4]}\n")
	
	# Fonction r2
	dico_liste = dico_liste(liste)
	pp.pprint(f"dico_liste : {dico_liste}\n")
	
	# Fonction r3
	dico_from_file = dico_from_file(liste)
	pp.pprint(dico_from_file)