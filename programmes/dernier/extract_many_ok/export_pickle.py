import pickle
from datastructures import Corpus

def write_pickle(corpus: Corpus, destination: str):
	with open(destination, "wb") as fout:
	    pickle.dump(corpus, fout)
	    return fout

def read_pickle(fichier_dump):
	return pickle.load(open(fichier_dump, 'rb'))
	
def save(corpus):
    return (corpus.__class__, corpus.__dict__)

def restore(cls, attributes):
    corpus = cls.__new__(cls)
    corpus.__dict__.update(attributes)
    return corpus