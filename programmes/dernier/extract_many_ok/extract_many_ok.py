from typing import Optional, List, Dict
import argparse
import sys
from datetime import date
from pathlib import Path
from xml.etree import ElementTree as et
import re
from tqdm import tqdm

import extract_un_fil as euf
from datastructures import Corpus, Article, Token
from export_json import write_json
from export_xml import write_xml
from export_pickle import  write_pickle
import analyse_trankit
import analyse_spacy 
import analyse_stanza

MONTHS = ["Jan",
		  "Feb",
		  "Mar",
		  "Apr",
		  "May",
		  "Jun",
		  "Jul",
		  "Aug", 
		  "Sep",
		  "Oct",
		  "Nov", 
		  "Dec"]

DAYS = [f"{x:02}" for x in range(1,32)]

CAT_CODES =  {	
			'une' : '0,2-3208,1-0,0', 
			'international' : '0,2-3210,1-0,0',
			'europe' : '0,2-3214,1-0,0', 
			'societe' :	'0,2-3224,1-0,0',
			'idees'	: '0,2-3232,1-0,0',
			'economie':	'0,2-3234,1-0,0',
			'actualite-medias':	'0,2-3236,1-0,0',
			'sport': '0,2-3242,1-0,0',
			'planete': '0,2-3244,1-0,0',
			'culture': '0,2-3246,1-0,0',
			'livres' : '0,2-3260,1-0,0',
			'cinema' : '0,2-3476,1-0,0',
			'voyage' : '0,2-3546,1-0,0',
			'technologies': '0,2-651865,1-0,0',
			'politique' : '0,57-0,64-823353,0',
			'sciences' : 'env_sciences'
			}

def categorie_of_ficname(ficname: str)-> Optional[str]:
	for nom, code in CAT_CODES.items():
		if code in ficname:
			return nom
	return None

def convert_month(m:str) -> int:
   return MONTHS.index(m) + 1

def parcours_path(corpus_dir:Path, categories: Optional[List[str]]=None, start_date: Optional[date]=None, end_date: Optional[date]=None):
	if categories is not None and len(categories) > 0:
		categories = [CAT_CODES[c] for c in categories]
	else:
		categories = CAT_CODES.values() # on prend tout

	for month_dir in corpus_dir.iterdir():
		if month_dir.name not in MONTHS:
			# on ignore les dossiers qui ne sont pas des mois
			continue
		m = convert_month(month_dir.name)
		for day_dir in month_dir.iterdir():
			if day_dir.name not in DAYS:
				# on ignore les dossiers qui ne sont pas des jours
				continue
			d = date.fromisoformat(f"2022-{m:02}-{day_dir.name}")
			if (start_date is None or start_date <= d) and (end_date is None or end_date >= d):
				for time_dir in day_dir.iterdir():
					if re.match(r"\d\d-\d\d-\d\d", time_dir.name):
						for fic in time_dir.iterdir():
							if fic.name.endswith(".xml") and any([c in fic.name for c in categories]):
								c = categorie_of_ficname(fic.name)
								yield fic, d.isoformat(), c


if __name__ == "__main__":
	parser = argparse.ArgumentParser("Ce pgm permet d'extraire les titres et descriptions des fils RSS du journal Le Monde. Il renvoie une structure de données étiquetées. La méthode de parsing et le format de sortie sont au choix.")
	parser.add_argument("-m", help="méthode de parsing : et (etree), re (regex) ou fp (feedparser), etree par défaut", default="et")
	parser.add_argument("-a", help="méthode d'analyse (trankit, stanza ou spacy)", default="spacy")
	parser.add_argument("-s", help="start date (iso format)", default="2022-01-01")
	parser.add_argument("-e", help="end date (iso format)", default="2023-01-01")
	parser.add_argument("-o", help="nom du fichier de sortie", required=True)
	parser.add_argument("-f", help="format du fichier de sortie, xml par défaut", default="xml")
	parser.add_argument("corpus_dir", help="répertoire du corpus de fils RSS")
	parser.add_argument("categories",nargs="*", help="catégories : une, international, europe, societe, idees, economie, actualite-media, sport, planete, culture, livres, cinema, voyage, technologies, politique, sciences")
	args = parser.parse_args()
	
	# Test de la méthode parsing
	if args.m not in ["et", "re", "fp"]:
		print("méthode de parsing non disponible", file=sys.stderr)
		sys.exit()
	
	# Test de la méthode analyse
	if args.a not in ["trankit", "stanza", "spacy"]:
		print("méthode d'analyse non disponible", file=sys.stderr)
		sys.exit()
		
	# Test du format de fichier sortie
	if args.f not in ["xml", "json", "pickle"]:
		print("format de sortie non disponible", file=sys.stderr)
		sys.exit()
	
	if args.m == 'et':
		func = euf.extract_et
	elif args.m == 're':
		func = euf.extract_re
	elif args.m == 'fp':
		func = euf.extract_feedparser		
		
	if args.a == 'trankit':
		analyse = analyse_trankit
	elif args.a == 'stanza':
		analyse = analyse_stanza
	elif args.a == 'spacy':
		analyse = analyse_spacy
	
	# création du corpus
	corpus = Corpus(args.categories, args.s, args.e, args.corpus_dir, [])
	for f, d, c in parcours_path(Path(args.corpus_dir), 
			start_date=date.fromisoformat(args.s),
			end_date=date.fromisoformat(args.e),
			categories=args.categories):
		# fichier par fichier, f: fichier, d: date, c: catégorie
		# article : objet Article(titre, description, date, categorie, [])
		for article in func(f, d, c):
			# parcours article par article
			corpus.articles.append(article)
	parser = analyse.create_parser()
	for a in tqdm(corpus.articles):
		a.analyse = analyse.analyse_article(parser, a).analyse

	# export du corpus aux différents formats
	if args.f == "xml":
		destination = args.o+".xml"
		write_xml(corpus, destination)
	elif args.f == "json":
		destination = args.o+".js"
		write_json(corpus, destination)
	elif args.f == "pickle":
		destination = args.o+"_pickle"
		write_pickle(corpus, destination)	