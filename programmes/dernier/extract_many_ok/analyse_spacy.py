import spacy
import fr_core_news_md
from datastructures import Article, Token

def create_parser():
	return spacy.load("fr_core_news_md")

def analyse_article(parser, article):
	desc = article.description
	date = article.date
	categorie = article.categorie
	p = parser(desc)
	list_analyse = []
	for t in p:
		token = Token(t.text, t.lemma_, t.pos_)
		list_analyse.append(token)
	article = Article(article.titre, desc, date, categorie, list_analyse)
	return article