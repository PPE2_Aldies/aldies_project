from typing import List
import argparse
import sys
import pyLDAvis
import pyLDAvis.gensim_models as gensimvis
import xml.etree.ElementTree as ET

def LdaModel_xml(fichier_xml) -> List:
	etiquettes = []
	# Parsing du fichier XML
	tree = ET.parse(fichier_xml)
	root = tree.getroot()

	liste_lemmes = []
	liste_formes = []
	liste_pos = []
	# ex. de token.attrib : {'forme': 'Un', 'pos': 'DET', 'lemme': 'un'}
	for article in root[1]:
		for token in article[2]:
			liste_lemmes.append(token.attrib.get('lemme'))
			liste_formes.append(token.attrib.get('forme'))
			liste_pos.append(token.attrib.get('pos'))
	etiquettes.append(liste_lemmes)
	etiquettes.append(liste_formes)
	etiquettes.append(liste_pos)
	return etiquettes
					
def LdaModel_json(fichier_json)->List:
	import json
	etiquettes = []
	# Opening JSON file
	f = open(fichier_json)
	# returns JSON object as 
	# a dictionary
	data = json.load(f)
	liste_lemmes = []
	liste_formes = []
	liste_pos = []
	jsonData = data["articles"]
	# récupérer dans les clés articles les valeurs de formes et de lemmes et les ajouter aux listes
	for x in jsonData:
		keys = x.keys()
		values = x.values()
		for e in values:
			for el in e:
				if len(el) > 1:
					lemme = el["lemme"]
					forme = el["forme"]
					pos = el["pos"]
					if lemme != ',' and lemme != '.' and lemme != "»":
						liste_lemmes.append(lemme)
						liste_formes.append(forme)
						liste_pos.append(pos)
	etiquettes.append(liste_lemmes)
	etiquettes.append(liste_formes)
	etiquettes.append(liste_pos)
	return etiquettes

def LdaModel_pickle(fichier_pickle):
	from export_pickle import read_pickle
	texte = read_pickle(fichier_pickle)
	etiquettes = []
	liste_articles=[]
	liste_lemmes = []
	liste_formes = []
	liste_pos = []
	for e in texte.articles:
		analyse = e.analyse
		for t in analyse:
			liste_lemmes.append(t.lemme)
			liste_formes.append(t.forme)
			liste_pos.append(t.pos)
	etiquettes.append(liste_lemmes)
	etiquettes.append(liste_formes)
	etiquettes.append(liste_pos)
	return etiquettes

def get_pos(etiquette, p):
	tri_formes = []
	tri_lemmes = []
	for i in range(len(etiquette[2])):
			pos = etiquette[2]
			formes = etiquette[0]
			lemmes= etiquette[1]
			if pos[i] == p:
				tri_formes.append(formes[i]) 
				tri_lemmes.append(lemmes[i])
	return tri_formes, tri_lemmes

def LdaModel_etiquettes(liste_etiquettes: List, nb:int, na:float):
	docs = liste_etiquettes
	for idx in range(len(docs)):
		docs[idx] = docs[idx].lower()  # Convert to lowercase.
		docs[idx]=docs[idx].split(' ')

	# Remove numbers, but not words that contain numbers.
	docs = [[token for token in doc if not token.isnumeric()] for doc in docs]
	# Remove words that are only one character.
	docs = [[token for token in doc if len(token) > 1] for doc in docs]
	
	# Compute bigrams.
	from gensim.models import Phrases
	#recuperer n gramme/ bigramme pour récupérer expressions figées possible sur gensim d'ou les add bigram pour recuperer que ceux qui sont interessant
	# Add bigrams and trigrams to docs (only ones that appear 20 times or more).
	bigram = Phrases(docs, min_count=20)
	for idx in range(len(docs)):
		for token in bigram[docs[idx]]:
			if '_' in token:
				# Token is a bigram, add to document.
				docs[idx].append(token)

	# Remove rare and common tokens.
	from gensim.corpora import Dictionary

	# Create a dictionary representation of the documents.
	dictionary = Dictionary(docs)

	# Filter out words that occur less than 20 documents, or more than 50% of the documents.
	dictionary.filter_extremes(no_below=nb, no_above=na)

	# Bag-of-words representation of the documents.
	corpus = [dictionary.doc2bow(doc) for doc in docs]

	print('Number of unique tokens: %d' % len(dictionary))
	print('Number of documents: %d' % len(corpus))

	# Train LDA model.
	from gensim.models import LdaModel

	# Set training parameters.
	num_topics = 10
	chunksize = 2000
	passes = 20
	iterations = 400
	eval_every = None  # Don't evaluate model perplexity, takes too much time.

	# Make an index to word dictionary.
	temp = dictionary[0]  # This is only to "load" the dictionary.
	id2word = dictionary.id2token

	model = LdaModel(
		corpus=corpus,
		id2word=id2word,
		chunksize=chunksize,
		alpha='auto',
		eta='auto',
		iterations=iterations,
		num_topics=num_topics,
		passes=passes,
		eval_every=eval_every
	)

	top_topics = model.top_topics(corpus)

	# Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
	avg_topic_coherence = sum([t[1] for t in top_topics]) / num_topics
	print('Average topic coherence: %.4f.' % avg_topic_coherence)

	from pprint import pprint
	pprint(top_topics)
	vis_data = gensimvis.prepare(model, corpus, dictionary)
	with open('visualisation.html', "w") as f:
		pyLDAvis.save_html(vis_data, f)


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-f", help="fichier de sortie de extract_many.py (sortie.js, sortie.xml ou sortie_pickle, sortie.js par défaut)", default="sortie.js")
	parser.add_argument("-p", help="pos choisi ('tous', 'noun', 'verb', 'noun+adj' ou 'adj', 'noun+adj' par défaut)", default='noun+adj')
	parser.add_argument("-d", help="type d'etiquette ('lemme' ou 'forme', 'forme' par défaut)", default="forme")
	parser.add_argument("-nb", help="filtre : ne garde PAS les mots qui apparaissent dans moins de nb documents, 20 par défaut)", type=int, default=20)
	parser.add_argument("-na", help="filtre : ne garde PAS les mots qui apparaissent dans plus de %%na documents, 0.5 par défaut)", type=float, default=0.5)
	#parser.add_argument("-o", help="génère la visualisation ldaviz et la sauvegarde dans le fichier html indiqué", default=None)
	args = parser.parse_args()
	
	# sélection du fichier à traiter
	if args.f == 'sortie.js':
		etiquettes = LdaModel_json(args.f)
	elif args.f == 'sortie.xml':
		etiquettes = LdaModel_xml(args.f)
	elif args.f == 'sortie_pickle':
		etiquettes = LdaModel_pickle(args.f)
	
	# choix des étiquettes à traiter
	if args.p == 'tous':
		etiquettes == etiquettes[0:1]
	elif args.p == 'noun':
		etiquettes = get_pos(etiquettes, 'NOUN')
	elif args.p == 'verb':
		etiquettes = get_pos(etiquettes, 'VERB')
	elif args.p == 'adj':
		etiquettes = get_pos(etiquettes, 'ADJ')
	elif args.p == 'noun+adj':
		etiquettes = get_pos(etiquettes, 'NOUN')+get_pos(etiquettes, 'ADJ')
	print(f"etiquettes : {etiquettes}\n")
	
	# choix des paramètres forme ou lemme, filtre not above, filtre not below
	nb = args.nb
	na = args.na
	if args.d == 'forme':
		func = LdaModel_etiquettes(etiquettes[1], nb, na )
	elif args.d == 'lemme':
		func = LdaModel_etiquettes(etiquettes[0], nb, na)