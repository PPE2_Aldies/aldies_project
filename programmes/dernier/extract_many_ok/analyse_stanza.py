import stanza
import json
from datastructures import Article, Token

def create_parser():
	return stanza.Pipeline(lang='fr', processors='tokenize, mwt, pos, lemma')

def analyse_article(parser, article):
	desc = article.description
	desc = str(desc)
	date = article.date
	categorie = article.categorie
	list_analyse = []
	#for e in desc:
	#nlp = stanza.Pipeline(lang='fr', processors='tokenize, mwt, pos, lemma')
	p = parser(desc)
	#liste = [f'{word.text+" "}, \t{word.lemma}\tpos: {word.pos}' for sent in doc.sentences for word in sent.words]
	for sent in p.sentences:
		for word in sent.words:
			token = Token(word.text, word.lemma, word.pos)
			list_analyse.append(token)
	article = Article(article.titre, desc, date, categorie, list_analyse)
	return article

def sortie_json(dico):
	sortie = json.dumps(dico)
	return sortie